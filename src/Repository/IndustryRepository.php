<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class IndustryRepository
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Repository
 */
class IndustryRepository extends EntityRepository
{

}