<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;


/**
 * Class VipRepository
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Repository
 */
class VipRepository extends EntityRepository
{

}