<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class DefaultController
 *
 * @author Jan Pache <jpache@dachcom.ch>
 * @package App\Controller
 */
class DefaultController extends Controller
{
    public function index(Request $request, $id)
    {
        return new Response($id);
        return $this->render('base.html.twig');
    }
}