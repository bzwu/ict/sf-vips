<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Industry
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\IndustryRepository")
 * @ORM\Table(name="industry")
 */
class Industry
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var Vip[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Vip", mappedBy="industries")
     */
    private $vips;

    /**
     * Industry constructor.
     */
    public function __construct()
    {
        $this->vips = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Industry
     */
    public function setName(string $name): Industry
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Vip[]|ArrayCollection
     */
    public function getVips()
    {
        return $this->vips;
    }

    /**
     * @param Vip[]|ArrayCollection $vips
     *
     * @return Industry
     */
    public function setVips($vips)
    {
        $this->vips = $vips;

        return $this;
    }
}