<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Vip
 *
 * @author  Jan Pache <jpache@dachcom.ch>
 * @package App\Entity
 * @ORM\Entity(repositoryClass="App\Repository\VipRepository")
 * @ORM\Table(name="vip")
 */
class Vip
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length="1")
     */
    private $gender;

    /**
     * @var Industry[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Industry", inversedBy="vips")
     * @ORM\JoinTable(name="vip_industry")
     */
    private $industries;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="vips")
     */
    private $country;

    /**
     * Vip constructor.
     */
    public function __construct()
    {
        $this->industries = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return Vip
     */
    public function setFirstname(string $firstname): Vip
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return Vip
     */
    public function setLastname(string $lastname): Vip
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return Vip
     */
    public function setGender(string $gender): Vip
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return Industry[]|ArrayCollection
     */
    public function getIndustries()
    {
        return $this->industries;
    }

    /**
     * @param Industry[]|ArrayCollection $industries
     *
     * @return Vip
     */
    public function setIndustries($industries)
    {
        $this->industries = $industries;

        return $this;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     * @param Country $country
     *
     * @return Vip
     */
    public function setCountry(Country $country): Vip
    {
        $this->country = $country;

        return $this;
    }
}